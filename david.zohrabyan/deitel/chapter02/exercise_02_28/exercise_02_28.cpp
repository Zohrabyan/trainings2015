/// This program enters number from five figures,divides number on separate figures and prints them separately from each other with three gaps between them    
#include <iostream> /// standard input output

/// function main begins program execution
int
main()
{
    int number, a, b, c, d, e; /// announcement of variable
 
    std::cout << "Enter number with five figures: " << std::endl; /// prompt user for data
    std::cin >> number; /// reads number from user into "number"
   
    if (number >= 10000) { /// compare number with "1000"
        if (number <= 99999) { /// compare number with "99999" 
            a = number / 10000; /// appropriate "a" the price division of "number" and "1000"
            b = (number / 1000) % 10; /// appropriate "b" the price division of "number" and "1000" with division on the module
            c = (number / 100) % 10; /// appropriate "c" the price division of "number" and "100" with division on the module
            d = (number / 10) % 10; /// appropriate "d" the price division of "number" and "10" with division on the module
            e = number % 10; /// appropriate "e" the price division on the module
            std::cout << " " << a; /// display the value of "a"
            std::cout << " " << b; /// display the value of "b"
            std::cout << " " << c; /// display the value of "c"
            std::cout << " " << d; /// display the value of "d"
            std::cout << " " << e << std::endl; /// display the value of "e";end line
        }
    }
    if (number > 99999) { /// compare number with "99999"
        std::cout << " You have entered wrong number, try again." << std::endl; /// output the text;end line
    }
    if (number < 10000) { /// compare number with "1000"
         std::cout << "You have entered wrong number, try again." << std::endl; /// output the text;end line
    }
    return 0; /// program ended successfully
} /// end function main


