/// Account class testing
#include <iostream>
#include "Account.hpp" /// Implementation of design’s Account

/// function main begins program execution
int
main()
{ 
    Account accountUser1(10); /// creates object of a class Account and gives the initial sum
    Account accountUser2(10); /// creates object of a class Account and gives the initial sum
    
    /// the objects causes function getBalance() and transfers the initial sums
    std::cout << "Account of User1 is: " << accountUser1.getBalance() << "\nAccount of User2 is: " << accountUser2.getBalance() << std::endl;
    
    /// the first object causes function credit() and adds the sum and display message
    accountUser1.credit(5);
    std::cout << "User1.After credit your balance is: " << accountUser1.getBalance() << std::endl;
    /// the first object causes function debit() and withdraws the sum and display message
    accountUser1.debit(12);
    std::cout << "User1.After debit your balance is: " << accountUser1.getBalance() << std::endl;
    
    /// the second object causes function credit() and adds the sum and display message
    accountUser2.credit(4);
    std::cout << "User2.After credit your balance is: " << accountUser2.getBalance() << std::endl;
    /// the second object causes function debit() and withdraws the sum and display message
    accountUser2.debit(30);
    std::cout << "User2.After debit your balance is: " << accountUser2.getBalance() << std::endl;
    
    return 0; /// function ended sucsessfully
} /// end function main
