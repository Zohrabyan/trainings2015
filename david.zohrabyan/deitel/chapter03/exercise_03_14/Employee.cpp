#include <iostream> /// Definitions Employee element functions
#include "Employee.hpp" /// Implementation of design’s Employee

Employee::Employee(std::string name, std::string surname, int salary) /// the constructor initializes parameter
{
    /// initializes a set-function call
    setName(name);
    setSurName(surname);
    setSalary(salary);
}

void
Employee::setName(std::string name) /// funcion set the name of Employee
{
    name_ = name; /// the member-variable appropriates name of parameter
}

void
Employee::setSurName(std::string surname)  /// funcion set the name of SurName
{
    surname_ = surname; /// the member-variable appropriates name of parameter
}

void
Employee::setSalary(int salary) /// funcion set the name of Salary
{
    if(salary >= 0) { /// checks the imported sum
        salary_ = salary; /// the member-variable appropriates value of parameter
    }
    if(salary < 0) { /// if the sum is smaller than zero
        salary_ = 0; /// appropriate zero to member-variable
        std::cout << "The entered " << salary << " is invalid. Now it is 0." << std::endl; /// output the message when function gives error
    }
}

std::string
Employee::getName() /// function get name of Employee
{
    return name_ ;
}

std::string
Employee::getSurName() /// function get name of SurName
{
    return surname_ ;
}

int
Employee::getSalary() /// function get name of Salary
{
    return salary_ ;
}

int
Employee::getAnnualWage() /// function get name of AnnualWage
{
    return 12 * salary_ ; /// considers a monthly salary
}

void
Employee::changeSalaryByPercent(int percent)
{ 
    setSalary(salary_ + salary_ * percent / 100); /// considers percent of a salary
}
