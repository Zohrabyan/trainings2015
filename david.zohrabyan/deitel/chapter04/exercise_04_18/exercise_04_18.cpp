#include <iostream>

int
main()
{
    int n = 1;
    std::cout << "\tN " << "\t10*N " << "\t100*N " << "\t1000*N \n";
    
    while(n < 6) 
    {
        std::cout << "\t" << 1 * n << "\t" << 10 * n << "\t" << 100 * n << "\t" << 1000 * n << std::endl;
        ++n;
    }
    
    return 0;
}
