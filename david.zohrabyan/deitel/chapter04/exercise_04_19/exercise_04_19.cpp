#include <iostream>

int
main()                                                                        
{
    double counter = 1, largest1 = 0, largest2 = 0;
    while (counter <= 10)
    {
        double number;
        std::cout << "Enter positive numbers and rough: ";
        std::cin >> number;
        if(number < 0) {
            std::cout << "Error 1: The entered number is not positive. " << std::endl;
            return 1;
        }
        if(number > largest1) {
            largest2 = largest1; 
            largest1 = number;
        } else if(number > largest2) {
            largest2 = number;
        }
        counter++;
    }
    
    std::cout << "Largest number is " << largest1 << std::endl;
    std::cout << "Largest2 number is " << largest2 << std::endl;   
   
    return 0;
}
