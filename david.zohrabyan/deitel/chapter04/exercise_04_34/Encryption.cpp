#include <iostream>
#include "Encryption.hpp"

void
Encryption::encode(int data)
{   
    int digit1 = (data + 7) % 10;   
    int digit2 = (data / 10 + 7) % 10;
    int digit3 = (data / 100 + 7) % 10;
    int digit4 = (data / 1000 + 7) % 10;
   
    std::cout << digit2 << digit1 << digit4 << digit3 << std::endl;   
}

void
Encryption::decode(int data)
{    
    int digit3 = (data + 10 - 7) % 10;
    int digit4 = (data / 10 + 10 - 7) % 10;
    int digit1 = (data / 100 + 10 - 7) % 10;
    int digit2 = (data / 1000 + 10 - 7) % 10;
    
    std::cout << digit4 << digit3 << digit2 << digit1 << std::endl; 
}
