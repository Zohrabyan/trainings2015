#include <iostream>

int
main()
{
    int quantity;
    std::cout << "Enter the quantity of numbers you want to compare: ";
    std::cin >> quantity;
    if(quantity < 0) {
        std::cerr << "Error 1. Quantity must be positive." << std::endl;
        return 1;
    } else if(0 == quantity) {
        std::cout << "Closing programm." << std::endl;
        return 0;
    } 
    
    int min = 2147483647;
    do {
        int number; 
        std::cout << "Enter number: ";
        std::cin >> number;
        if(number <= min) {
            min = number;
        }
        --quantity;
    } while(0 != quantity);
    std::cout << "Min. number is " << min << std::endl;

    return 0;
}

