#ifndef __ACCOUNT_H__
#define __ACCOUNT_H__
/// Account.hpp
/// The Account class definition presents public interface
/// ot the class.

/// Account class definition
class Account
{
public:
    Account(int balance); /// constructor initalize balance_
    void debit(int money); ///  
    void credit(int amount); ///
    int getBalance(); /// 
private:
    int balance_; /// Account balance
}; /// end class  Account

#endif /// __ACCOUNT_H__
