/// Employee.cpp
/// Impliementations of the Employee member-function defintions

#include "Employee.hpp" /// include defintion of class Employee
#include <iostream> /// allows program to perform input and output

/// constructor validate and initialize data members
Employee::Employee(std::string firstName, std::string lastName, int salary)
{
    setFirstName(firstName); /// validate and store firstname
    setLastName(lastName); /// validate and store lastname
    setSalary(salary); /// validate and store salary
} /// end Employee constructor

/// function sets firstname
void
Employee::setFirstName(std::string firstName)
{
    firstName_ = firstName;
} /// end function setFirstName

/// function to get firstname
std::string
Employee::getFirstName()
{
    return firstName_; /// return object's firstname
} /// end function getFirstName

/// function sets lastname
void
Employee::setLastName(std::string lastName)
{
    lastName_ = lastName;
} /// end function setLastName

/// function to get lastname
std::string
Employee::getLastName()
{
    return lastName_; /// return object's lastname
} /// end function getLastName

/// function sets salary
void
Employee::setSalary(int salary)
{
    if(salary < 0) { /// if salary smaller 0
        salary_ = 0;
        std::cout << "The salary can't be smaller 0. Salary reset 0.\n";
    } /// end if
    if(salary >= 0) { /// if salary is valid
        salary_ = salary;
    } /// end if
} /// end funtion setSalary

/// function to get salary
int
Employee::getSalary()
{
    return salary_; /// return object's salary
} /// end function getSalary
