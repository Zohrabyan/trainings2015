#ifndef __EMPLOYEE_HPP__
#define __EMPLOYEE_HPP__
/// Employee.hpp
/// Employee class definition 
/// the public interface of the class

#include <string> /// program uses standard c++ string class

/// Employee class defintion
class Employee
{
public:
    /// constructor initialize data members
    Employee(std::string firstName, std::string lastName, int salary);
    void setFirstName(std::string firstName); /// sets the firstname
    std::string getFirstName(); /// gets the firstname
    void setLastName(std::string lastName); /// sets the lastname
    std::string getLastName(); /// gets the lastname
    void setSalary(int salary); /// sets the salary
    int getSalary(); /// gets the salary
private:
    std::string firstName_;
    std::string lastName_;
    int salary_;
}; /// end class Employee

#endif /// __EMPLOYEE_HPP__

