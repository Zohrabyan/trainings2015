/// main.cpp
/// Test the Date class

#include "Date.hpp" /// include definition of class
#include <iostream> /// allows program to perform input and output

/// function main begins program execution
int
main()
{
    /// initialize Date object
    Date date(5, 7, 2012);
    date.displayDate();
    std::cout << "\nTest class error handling!" << std::endl;
    date.setMonth(15);
    date.setDay(-3);
    date.setYear(251212);
    date.displayDate();
    std::cout << std::endl;
    return 0; /// program ends successfully
} /// end function main
