/// The program finds largest number during inputing data

#include <iostream> /// allows program to perform input and output
#include <iomanip> /// allows program to perform stream manipulator

/// function main begins program execution
int
main()
{
    /// initialize data
    int counter = 1;
    int largest = -2147483647;
    /// while counter is smaller then 10
    while (counter <= 10) {        
        int number = 0;
        /// prompt user to enter number
        std::cout << "Enter number " << counter << ": ";
        std::cin >> number; /// input number

        /// if number is greather then largest
        if(number > largest) {
            largest = number; /// assign largest to number
        }

        ++counter; /// increment number
    }
    /// print largest
    std::cout << "Largest: " << largest << std::endl;

    return 0; /// program ends successfully
} /// end function main
