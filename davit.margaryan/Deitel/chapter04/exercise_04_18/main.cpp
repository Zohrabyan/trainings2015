///

#include <iostream> /// allows program to perform input and output
#include <iomanip> /// allows program to perform stream manipulator

/// function main begins program execution
int
main()
{
    int counter = 1;

    std::cout << "N\t10*N\t100*N\t1000*N\n";
    while (counter <= 5) {
        std::cout << counter << '\t' 
                  << 10 * counter << '\t'
                  << 100 * counter << '\t' 
                  << 1000 * counter << '\n';
        ++counter;
    }
    return 0; /// program ends successfully
} /// end function main
