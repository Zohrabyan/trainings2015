#include <iostream>

int 
main()
{
    int id = 0;
    double balance, credit, newBalance;

    while(id != -1)
    {
        std::cout << "\nInput id number (-1, if you'r finished): ";
        std::cin  >>  id;
        if (-1 == id) {
            return 1;
        }
		
        std::cout << "\nInput the initial balance: ";
        std::cin  >> balance;
		
        double expense;
        std::cout <<  "\nInput the expense: ";
        std::cin  >> expense;
        if(0 > expense){
            std::cout << "Input not valid, closing program";
            return 1;
        };
        double profit;
        std::cout <<  "\nInput the profit: ";
        std::cin  >> profit;
        if(0 > profit){
            std::cout << "Input not valid, closing program";
            return 2;
        };
        std::cout <<  "\nInput limit of credit: ";
        std::cin  >> credit;
        if (0 > credit){
            std::cout << "Input not valid, closing program";
            return 3;
        }
        newBalance = balance + expense - profit;
        std::cout << "\nNew balance: " << newBalance;
        if (newBalance > credit){
            std::cout << "\nId: " << id;
            std::cout << "\nCredit limit: " << credit;
            std::cout << "\nBalance: " << newBalance << std::endl;
            std::cout << "\nOut of credit limit" << std::endl;
        };
    };

    return 0;
}
