#include <iostream>

int
main()
{
    int number;
    std::cout << "Input 5 digit number(12345): ";
    std::cin >> number;
    if (0 >= number) {
        std::cout << "\nInput not valid, example 12345" << std::endl;
        return 1;
    } else if (9999 >= number) {
        std::cout << "\nInput not valid, your number is too high or too small" << std::endl;
        return 1;
    } else if (99999 < number) {
        std::cout << "\nInput not valid, your number is too high or too small" << std::endl;
        return 1;
    }
    int digit1 = number / 10000;
    int digit2 = (number / 1000) % 10;
    int digit4 = (number / 10) % 10;
    int digit5 = number % 10;
    if (digit1 == digit5) {
        if (digit2 == digit4) {
            std::cout << "This number is Palindrome" << std::endl;
            return 0;
        }
    }
    std::cout << "This number is not Palindrome" << std::endl;
    return 0; 
}
