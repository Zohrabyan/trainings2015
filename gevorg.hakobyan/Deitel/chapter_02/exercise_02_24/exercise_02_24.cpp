#include <iostream> 

int 
main() 
{
    int a;
    
    std::cout << "Enter number: ";
    std::cin >> a ;
	
    if (a % 2 == 0)
        std::cout << "The number is even\n" ;

    if (a % 2 != 0)
        std::cout << "The number is odd\n" ;

    return 0;
}
