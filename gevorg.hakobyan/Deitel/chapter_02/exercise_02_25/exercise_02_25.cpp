#include <iostream> 

int 
main() 
{     
    int a;
    int b;
	
    std::cout << "Enter 2 numbers: ";
    std::cin >> a >> b ;
	
    if (a != 0) {
        if ((b % a) == 0)
            std::cout << "A multiple\n" ;
	
        if ((b % a) != 0)
            std::cout << "It is not a multiple\n" ;
    }
	
    if (a == 0) 
        std::cout << "Try again\n";

    return 0;
} 
