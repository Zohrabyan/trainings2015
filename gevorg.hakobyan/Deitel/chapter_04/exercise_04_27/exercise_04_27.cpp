#include <iostream>

int
main()
{
    int decimal = 0, degree = 1, number;
    
    std::cout << "Enter the binary code: ";
    std::cin  >> number;

    if (number < 0){
        std::cout << "Error 1: The number should be non-negative.\nTry again." << std::endl;
        return 1;
    }
    
    while (number > 0){
        int binaryDigit = number % 10;
        int decimalDigit = binaryDigit * degree;
        decimal += decimalDigit;
        degree *= 2;
        
        if (binaryDigit > 1){
            std::cout << "Error 2: The number should be binary code.\nTry again." << std::endl;
            return 2;
        }
        number /= 10;
    } 

    std::cout << "Corresponding decimal code: " << decimal << std::endl;

    return 0;
}
