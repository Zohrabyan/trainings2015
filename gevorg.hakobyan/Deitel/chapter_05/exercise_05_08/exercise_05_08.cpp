#include <iostream>

int
main()
{
    int amount; 
    std::cout << "Enter the amount: ";
    std::cin  >> amount;
    
    if (amount <= 0){
        std::cout << "Error 1: Amount should be positive.\nTry again." << std::endl;
        return 1;
    }

    int min = 2147483647;
    for (int counter = 1; counter <= amount; ++counter){                   
        int number;        
        std::cout << "Enter the number: ";
        std::cin  >> number;       
                    
        if (number < min){
            min = number;
        }              
    }

    std::cout << "Min = " << min << std::endl;

    return 0;
}   
