#include <iostream>

int
main()
{
    int composition = 1;
    std::cout << "\tNumber" << "\tFactorial" << std::endl;

    for(int number = 1; number <= 5; ++number){
        composition *= number;

        std::cout << "\t" << number << "!" << "\t" << composition << std::endl;
    }

    return 0;
}
