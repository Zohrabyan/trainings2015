#include <iostream>
#include <iomanip>
#include <cmath>

int
main()
{
    int amount = 100000;

    std::cout << "Year" << std::setw(24) << "Amount on deposit" << std::endl;
    
    for (int year = 1; year <= 10; ++year){      
        amount = amount * 105 / 100;
        int dollar = amount / 100;
        int cent = amount % 100;
        std::cout << std::setw(4) << year << std::setw(12) << dollar << "$" 
            << std::setw(6) << cent << " cent" << std::endl;
    }

    return 0;
}
