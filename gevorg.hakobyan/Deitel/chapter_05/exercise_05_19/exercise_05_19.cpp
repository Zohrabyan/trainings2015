#include <iostream>

int
main()
{
    int goldenPi = 314159, counter = 1, sign = 1, quantity = 0;
    double pi = 0.0;
    for (int sigma1 = 100, sigma2 = 1000; sigma2 >= 1; sigma2 /= 10, sigma1 *= 10){
        int  currentGoldenPi = goldenPi / sigma2;
        do{
            pi += 4.0 / counter * sign; 
            sign *= -1;
            counter += 2;
            ++quantity;
        } while (pi * sigma1 - currentGoldenPi >= 0.1 || pi * sigma1 - currentGoldenPi < 0);
        
        std::cout << "pi: " << pi << "\tquantity: " << quantity << std::endl;
    }    

    return 0;
}
