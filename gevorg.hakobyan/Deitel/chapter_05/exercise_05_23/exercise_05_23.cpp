#include <iostream>
#include <cmath>

int
main()
{
    
    for (int horizontal = 0; horizontal <= 8; ++horizontal){
        int equalizing = std::abs(4 - horizontal);                 
        for (int vertical = 0; vertical <= 8; ++vertical){
            if (vertical >= equalizing && vertical <= 8 - equalizing) {
                std::cout << "*";
            } else {
                std::cout << " ";
            }
        }
        std::cout << std::endl;
    }

    return 0;
}
