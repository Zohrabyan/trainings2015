#include <iostream>

int 
main()
{ 
    int R;

    std::cout << "Enter radius: ";
    std::cin >> R;

    std::cout << "Diameter " << 2 * R << "\n";
    std::cout << "Circle " << 2 * 3.14159 * R << "\n";
    std::cout << "Area " << 3.14159 * R * R << "\n";

    return 0;
}
