#include "Account.hpp"
#include <iostream>

Account::Account(int balance)
{
    if (balance < 0)
    {
        balance_ = 0;
        std::cout << "Invalid Balance ! " << std::endl;
    }
    
    if (balance >= 0)
    {
        balance_ = balance;
    }
}

void 
Account::credit(int credit)
{
    balance_ += credit;
}

void
Account::debit(int debit)
{
    if (debit <= balance_)
    {
        balance_ -= debit;
    }

    if (debit > balance_)
    {
        std::cout << "The requested amount exceeds the account balance" << std::endl;
    }
}

int
Account::getBalance()
{
    return balance_;
}
