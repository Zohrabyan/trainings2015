#include "Employee.hpp"
#include <iostream>

int
main()
{
    Employee employee1("Ktuc", "Chxkikyan", 150000);
    Employee employee2("Hafo", "Shuxuryan", 200000);

    std::cout << "First employee name: "
              << employee1.getName() << '\n'
              << "First employee surname: "
              << employee1.getSurname() << '\n'
              << "First employee yearly salary: "
              << employee1.getSalary() * 12 << '\n'
              << "\nSecond employee name: "
              << employee2.getName() << '\n'
              << "Second employee surname: "
              << employee2.getSurname() << '\n'
              << "Second employee yearly salary: "
              << employee2.getSalary() * 12 << std::endl;

    employee1.setSalary(employee1.getSalary() + employee1.getSalary() * 0.1);
    employee2.setSalary(employee2.getSalary() + employee2.getSalary() * 0.1);
    std::cout << "\nFirst employee yearly salary after 10% raising: "
              << employee1.getSalary() * 12  << '\n'
              << "Second employee yearly salary after 10% raising: "
              << employee2.getSalary() * 12  << std::endl;

    return 0;
}
