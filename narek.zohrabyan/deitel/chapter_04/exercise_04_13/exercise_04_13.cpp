#include <iostream>
#include <iomanip>

int 
main()
{ 
    double miles, totalMiles = 0.0, totalOil = 0.0;
    std::cout << "Enter traveled miles (-1, if the input is completed): ";
    std::cin >> miles;
    while (miles != -1) {
        if (miles < -1 ) {
            std::cout << "Invalid traveled miles. Try Again!" << std::endl;
            return 1;
        }
        std::cout << "Enter oil expense: ";
        double oil;
        std::cin >> oil;
        if (oil <= 0) {
            std::cout << "Invalid oil expense. Try Again!" << std::endl;
            return 2;
        }
        double total = miles / oil;
        std::cout << "Miles/gallon for this fueling: " << std::setprecision(6) << std::fixed << total << std::endl;
        totalMiles += miles;
        totalOil += oil;
        double overall = totalMiles / totalOil;
        std::cout << "The total value mile/gallon: " << std::setprecision(6) << std::fixed << overall << "\n" << std::endl;
        std::cout << "Enter traveled miles (-1, if the input is completed ): ";
        std::cin >> miles; 
    } 
    return 0;
}
