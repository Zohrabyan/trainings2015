#include <iostream>
#include <iomanip>

int 
main()
{
    int number;
    std::cout << "Enter account number (-1, if the input is completed): ";
    std::cin >> number;
    while (number != -1) {
        if (number < -1) {
            std::cout << "Account number is invalid. Try again!" << std::endl;
            return 1;
        }
        double balance;
        std::cout << "Enter the initial balance: ";
        std::cin >> balance;
        if (balance < 0) {
            std::cout << "Initial balance is invalid. Try again!" << std::endl;
            return 2;
        }
        double expense;
        std::cout << "Enter the sum of expenses: ";
        std::cin >> expense;
        if (expense < 0) {
            std::cout << "The sum of expense is invalid. Try again!" << std::endl;
            return 3;
        }
        double parish;
        std::cout << "Enter the sum of parishes: ";
        std::cin >> parish;
        if (parish < 0) {
            std::cout << "The sum of parishes is invalid. Try again!" << std::endl;
            return 4;
        }
        double credit;
        std::cout << "Enter credit limit: ";
        std::cin >> credit;
        if (credit <= 0) {
            std::cout << "The credit limit is invalid. Try again!" << std::endl;
            return 5;
        }
        double newbalance = balance + expense - parish;
        std::cout << "New balance: " << std::setprecision(2) << std::fixed << newbalance << std::endl;
        if (newbalance > credit) {
            std::cout << "Account: " << std::setprecision(2) << std::fixed << number << std::endl;
            std::cout << "Credit limit: " << std::setprecision(2) << std::fixed << credit << std::endl;
            std::cout << "Balance: " << std::setprecision(2) << std::fixed << balance << std::endl;
            std::cout << "The limit of credit exceeded" << std::endl;
        }
        std::cout << "\n\nEnter account number (-1, if the input is completed): ";
        std::cin >> number;
    }
    return 0;
}
