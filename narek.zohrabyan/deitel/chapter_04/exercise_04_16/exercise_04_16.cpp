#include <iostream>
#include <iomanip>

int
main()
{
    int time;
    std::cout << "Enter working time (-1, if the input is completed): ";
    std::cin >> time;
    while (time != -1) {
        if (time < 0) {
            std::cout << "Invalid working time. Try again!" << std::endl;
            return 1;
        }
        double rate;
        std::cout << "Enter hourly rate of the employee ($ 00.00): ";
        std::cin >> rate;
        if (rate <= 0) {
            std::cout << "Invalid hourly rate. Try again!" << std::endl;
            return 2;
        } 
        double salary = rate * time;
        if (time > 40) {
            salary += (time - 40) * (rate / 2);
        } 
        std::cout << "Salary: $" << std::setprecision(2) << std::fixed << salary << std::endl;

        std::cout << "\n\nEnter working time (-1, if the input is completed): ";
        std::cin >> time;
    }
    
    return 0;
}
