#include <iostream>

int
main()
{
    int number = 1;
    std::cout << "\n\tN\t10*N\t100*N\t1000*N\n\n";
    while (number < 6) {
        std::cout << "\t" << number << "\t" << 10 * number << "\t" << 100 * number << "\t" << 1000 * number << std::endl;
        ++number;
    }

    return 0;
}
