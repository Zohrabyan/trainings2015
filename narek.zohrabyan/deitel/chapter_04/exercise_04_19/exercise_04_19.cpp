#include <iostream>

int
main()
{
    int counter = 0, largest1 = -2147483648, largest2 = -2147483648;
    while (counter < 10) {
        int number;
        std::cout << "\nEnter number: ";
        std::cin >> number;
        if (number > largest1) {
            largest2 = largest1;
            largest1 = number;
        } else if (number > largest2) {
            largest2 = number;
        }
        ++counter;
    }
    std::cout << "\nFirst largest number is: " << largest1 
              << "\nSecond largest number is: " << largest2 << std::endl;

    return 0;
}
