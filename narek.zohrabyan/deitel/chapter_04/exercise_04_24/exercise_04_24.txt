a)
if (8 == y) {
    if (5 == x) {
        std::cout << "@@@@@" << std::endl;
    }
} else {
    std::cout << "#####" << std::endl;
}
std::cout << "$$$$$" << std::endl;
std::cout << "&&&&&" << std::endl;

b)
if (8 == y) {
    if (5 == x) {
        std::cout << "@@@@@" << std::endl;
    }
} else {
    std::cout << "#####" << std::endl;
    std::cout << "$$$$$" << std::endl;
    std::cout << "&&&&&" << std::endl;
}
    
c)
if (8 == y) {
    if (5 == x) {
        std::cout << "@@@@@" << std::endl;
    }
} else {
    std::cout << "#####" << std::endl;
    std::cout << "$$$$$" << std::endl;
}
std::cout << "&&&&&" << std::endl;
 
d)
if (8 == y) {
    if (5 == x) {
        std::cout << "@@@@@" << std::endl;
    }
} else {
    std::cout << "#####" << std::endl;
    std::cout << "$$$$$" << std::endl;
    std::cout << "&&&&&" << std::endl;
}
